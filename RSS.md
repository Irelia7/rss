

小组成员

3019244192 王天祥  
3019244193 焦宏通  
3019244207 赵万旭  
3019201120 樊兴宇  

RSS0824

Resources and sharing system

## 一、 实践整体要求：

题目为移动互联网方向应用。开发平台要求：

1. 前后端分离架构。项目由三部分组成：服务器后端、微机浏览器前端、移动手机端。前后端接口采用RESTful风格，以JSON作为数据格式。

2. 后端建议使用的框架：java spring boot、python flask/django、node express/koa

3. 微机浏览器端建议使用的框架：angular、react、vue

4. 移动手机端建议使用的框架：微信小程序、flutter

 

## 二、 通用题目要求（本要求适用于后面所有的题目）

1. 服务器后端的功能需求

   a) 虽然用户通过前端发起业务请求，但是处理业务逻辑应该主要在后端代码中执行。因为前端可能有很多种（如微机浏览器前端、微信小程序，手机APP，甚至是其他业务系统等），所以具体的业务逻辑处理应该由统一的后端处理。

   b) 服务器后端负责与数据库交互，完成数据持久化。

2. 用户身份认证以手机号为用户的标识，可以通过手机短信认证来重置密码。手机号与社交APP账号（如微信、QQ、微博账号）进行绑定并支持第三方登录是推荐的选做内容。

 

## 三、 项目提交的材料：

1. 文档：《软件需求规格说明书》、《软件设计文档》、《软件测试文档》、《软件部署使用文档》、《人员分工说明书》

2. 代码：包括前端代码、后端代码和数据库信息。

3. 录像：程序的演示录像



## 四、 资源共享管理系统

1. 应用场景介绍。我们身边都有东西可以租借出去，一方面提高物品的利用率，另一方面也可以有一些收入。例如：

   a) 会议室可以提供预订，例如以半小时为时间段计时收费。

   b) 羽毛球场可以提供预订，例如以一小时为时间段计时收费。

   c) 我的宿舍有一套WII游戏机，可以租借给别人用，以一小时为时间段计时收费。

   d) 我的课题组有一台投影仪，平时一周只使用一天次，其他时间可以租借出去。

   e) 我擅长打网球，我把自己“租借”出去当陪练，每周五下午1点到5点，每小时收费XX元。

2. 资源信息说明

   a) 资源所有人（即发布者）

   b) 资源开放时间段（例如今天晚上6点到9点）

   c) 资源计费单位及单价（例如以半小时计费，每半小时20元）

   d) 资源的并发使用数（例如，一个羽毛球场只能供一组人员使用，但是一次郊游租来的大巴车上的座位可以有20个人来预订同行）

3. 订单信息说明

   a) 资源所有者

   b) 预订者

   c) 订单状态。订单成功分二种情况，一是收费的资源，支付完成订单即成功。二是不收费的资源（例如内部使用的会议室，只是预订不交费），资源所有者批准即成功。订单提交后一般锁定一段时间，例如半小时，如果到时间后交易不成功，则交易失败，解除锁定。

4. 微信小程序的基本功能需求

   a) 浏览或搜索资源，对心怡的资源可以下单预订。

   b) 查看自己的订单状态

   c) 发布/撤销自己的资源（简单情况）

   d) 资源所有者审批通过订单（见订单状态的说明，不收费的资源一般要审批）

5. 电脑浏览器前端的基本功能需求

   a) 批量发布资源。发布资源时指定周期性重复，例如每周一下午2点到5点，连续8周。

   b) 查看自己拥有的资源的情况。



## 五、连接gitee远程仓库地址

#### 快速设置— 如果你知道该怎么操作，直接使用下面的地址

HTTPS

SSH https://gitee.com/Irelia7/rss.git

 

我们强烈建议所有的git仓库都有一个`README`, `LICENSE`, `.gitignore`文件



初始化 readme 文件





Git入门？查看 [帮助](https://gitee.com/oschina/git-osc/wikis/帮助) , [Visual Studio](https://gitee.com/help/articles/4118) / [TortoiseGit](http://my.oschina.net/longxuu/blog/141699) / [Eclipse](https://gitee.com/help/articles/4119) / [Xcode](http://my.oschina.net/zxs/blog/142544) 下如何连接本站, [如何导入仓库](http://www.oschina.net/question/82993_133520)

#### 简易的命令行入门教程:

Git 全局设置:

```bash
git config --global user.name "Irelia"
git config --global user.email "2958887979@qq.com"
```

创建 git 仓库:

```bash
mkdir rss
cd rss
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/Irelia7/rss.git
git push -u origin master
```

已有仓库?

```bash
cd existing_git_repo
git remote add origin https://gitee.com/Irelia7/rss.git
git push -u origin master
```

![image-20210826140531194](../微信下载文件/WeChat Files/wxid_qudwuxnlany722/FileStorage/File/微信下载文件/WeChat Files/wxid_qudwuxnlany722/FileStorage/File/2021-09/RSS.assets/image-20210826140531194.png)



![image-20210826140623204](../微信下载文件/WeChat Files/wxid_qudwuxnlany722/FileStorage/File/微信下载文件/WeChat Files/wxid_qudwuxnlany722/FileStorage/File/2021-09/RSS.assets/image-20210826140623204.png)



## 需求分析

### 1.简介

​	1.1背景

​	1.2定义缩略词

​	1.3约束

​	1.4参考资料

### 2.目标，涉众分析和范围

​	2.1目标

​	2.2涉众分析

​	2.3范围

### 3.业务概念分析

​	3.1概述

​	3.2概念一览

​	3.3...各种业务细分

## 4.业务流程分析

​	4.1概述

​	4.2....各个业务的流程分析

### 5.功能性分析

​	5.1执行者分析

​	5.2总用例图

​	5.3普通员工的用例

​	表格展示

​	5.4行政部员工等用例

​	5.7其他功能性需求

### 6.非功能性需求

​	6.1系统架构

​	6.2接口

​	6.3安全性

​	6.4性能

​	6.5界面

### 7.附录

### 8.版本修订记录





#### 战略分析

#### 需要分析

#### 业务分析

#### 需求分析



# 接口

## RESTful 风格 API 接口文档模板

# 注册


### 1 接口名称

注册

### 2 接口描述

- 用户信息注册
- 用户可以通过手机号进行注册
- 同一个手机只能注册一个账号

### 3 请求地址

```
{apiAddress}/api/v1/users
```

### 4 请求方式

**POST**

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名      | 必选 | 类型   | 限制条件        | 说明     |
| ----------- | ---- | ------ | --------------- | -------- |
| userPro     | 否   | string | 50 > length > 0 | 密保问题 |
| userAns     | 否   | string | 50 > length > 0 | 密保答案 |
| phoneNumber | 是   | string | length = 11     | 用户账号 |
| password    | 是   | string | 8 < length < 20 | 密码     |
| nickname    | 是   | string | 4 < length < 20 | 名称     |
| stuID       | 是   | string | length = 10     | 学号     |

### 6 返回示例

```
{
    "code": 200,  // 状态码
    "msg": "成功",  // 提示信息
    "data": null  // 返回内容
}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](https://www.runoob.com/http/http-status-codes.html)

# 登录登出

### 1 接口名称

用户登录登出接口

### 2 接口描述

- 用户通过手机号和密码登录或登出

### 3 请求地址

```
{apiAddress}/api/v1/users/
```

### 4 请求方式

**GET**

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名      | 必选 | 类型   | 限制条件        | 说明                 |
| ----------- | ---- | ------ | --------------- | -------------------- |
| type        | 是   | int    | 0\|1            | 0表示登录，1表示登出 |
| phoneNumber | 否   | string | length = 11     | 用户账号             |
| password    | 否   | string | 8 < length < 20 | 密码                 |

### 6 返回示例

```
{
    "code": 200,  // 状态码
    "msg": "成功",  // 提示信息
    "data": null  // 返回内容
}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 发布资源

### 1 接口名称

发布资源请求接口

### 2 接口描述

- 用户发布资源

### 3 请求地址

```
{apiAddress}/api/v1/users/0/items
```

### 4 请求方式

POST

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名         | 必选 | 类型   | 限制条件                     | 说明                         |
| -------------- | ---- | ------ | ---------------------------- | ---------------------------- |
| itemsPicture   | 是   | string |                              | 图片地址                     |
| itemsType      | 是   | int    | 公共资源\|个人资源\|个人能力 | 公共资源\|个人资源\|个人能力 |
| itemsName      | 是   | string | 0<length<30                  | 资源名称                     |
| itemsPrice     | 是   | double | >=0                          | 单价                         |
| itemsMaxNum    | 是   | int    | >0                           | 最大可并行数量               |
| itemsStartTime | 是   | date   |                              | 租赁起始时间                 |
| itemsEndTime   | 是   | date   |                              | 租赁结束时间                 |
| remark         | 否   | string | 0< length <50                | 备注                         |

### 6 返回示例

```
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data":null // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 查看个人信息

### 1 接口名称

查看个人信息

### 2 接口描述

- 查看个人信息

### 3 请求地址

```
{apiAddress}/api/v1/users/0/
```

### 4 请求方式

**GET**

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

null

### 6 返回示例

```
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": {    "nickname"：    "stuID":    "username": , "userPictuer":    }  // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 修改个人信息

### 1 接口名称

个人信息修改

### 2 接口描述

- 个人信息修改

### 3 请求地址

```
{apiAddress}/api/v1/users/0/
```

### 4 请求方式

PUT

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名         | 必选 | 类型   | 限制条件 | 说明       |
| -------------- | ---- | ------ | -------- | ---------- |
| newPhoneNumber | 否   | string |          | 用户电话号 |
| password       | 是   | string |          | 登陆密码   |
| stuID          | 否   | string |          | 学号       |
| nickname       | 否   | string |          | 昵称       |

### 6 返回示例

```
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": null  // 返回内容}
```

{
    "code": 200,  // 状态码
    "msg": "成功",  // 提示信息
    "data": null  // 返回内容
}

### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 忘记密码？修改密码

### 1 接口名称

个人信息修改

### 2 接口描述

- 个人信息修改

### 3 请求地址

```
{apiAddress}/api/v1/users/0/
```

### 4 请求方式

PATCH

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名      | 必选       | 类型   | 限制条件 | 说明               |
| ----------- | ---------- | ------ | -------- | ------------------ |
| username    | 忘记密码   | string |          | 用户电话号         |
| password    | 修改密码   | string |          | 登陆密码           |
| newPassword | 忘记密码   | string |          | 新的密码           |
| password2   | 确认密码   | string |          | 确认密码           |
| typecode    | 是         | int    | 0\|1     | 找回密码\|修改密码 |
| userAns     | 忘记密码T2 | string |          | 密保答案           |

### 6 返回示例

```
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": null  // 返回内容}
```

{
    "code": 200,  // 状态码
    "msg": "成功",  // 提示信息
    "data": null  // 返回内容
}

### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 撤销资源

### 1 接口名称

撤销资源

### 2 接口描述

- 用户撤销已发布资源

### 3 请求地址

```
{apiAddress}/api/v1/users/0/items/:itemsID
```

### 4 请求方式

DELETE

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名  | 必选 | 类型   | 限制条件 | 说明         |
| ------- | ---- | ------ | -------- | ------------ |
| itemsID | 是   | string |          | 物品唯一标识 |

### 6 返回示例

```java
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": null // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 查看发布资源

### 1 接口名称

查看已发布资源

### 2 接口描述

- 用户查看已发布资源

### 3 请求地址

```
{apiAddress}/api/v1/users/0/items/
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 URL 参数

| 参数名   | 必选 | 类型   | 限制条件       | 说明               |
| -------- | ---- | ------ | -------------- | ------------------ |
| limit    | 是   | int    |                | 单页展示物品数量   |
| page     | 是   | int    |                | 访问的页面数       |
| typecode | 否   | string | 上架中\|已出售 | 需要获得的商品状态 |

### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "totalPage": 100,    "data": [    {    "itemsID":"11",    "itemsName":"dksfh",    "itemsPrice":10.0,    "itemsNumber":1,    "itemsMaxNum":10,    "remark":"hhh"    } ,        ] // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 查看某个发布资源

### 1 接口名称

查看某个已发布资源

### 2 接口描述

- 用户查看某个已发布资源

### 3 请求地址

```
{apiAddress}/api/v1/items/:itemsID
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 URL 参数

| 参数名  | 必选 | 类型   | 限制条件 | 说明         |
| ------- | ---- | ------ | -------- | ------------ |
| itemsID | 是   | string |          | 物品唯一标识 |

### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息   
 "data":     {    
     "itemsName":"dksfh", 
     "itemsPrice":10.0,   
     "itemsNumber":1,   
     "itemsMaxNum":10,   
     "itemsStartTime":"",    
     "itemsEndTime":"",   
     "remark":"hhh",   
     "itemsType": "私有"    }        // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](

# 查看所有订单 

### 1 接口名称

查看所有订单

### 2 接口描述

- 查看所有订单

### 3 请求地址

```
{apiAddress}/api/v1/users/0/orders
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名 | 必选 | 类型 | 限制条件   | 说明                           |
| ------ | ---- | ---- | ---------- | ------------------------------ |
| type   | 是   | int  | 0\|1\|2\|3 | 全部\|未付款\|未审批\|待我审批 |



### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": [    {    	"ordersID":"11",        "buyerPhone":"159...",        "sellerPhone":"198..",        "orderStatus":0,        "orderPrice":10.0    } ,    ...        {                    }        ] // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](查看某个发布资源

# 查看某个已存在订单

### 1 接口名称

查看某个已存在订单

### 2 接口描述

- 用户查看某个已存在订单

### 3 请求地址

```
{apiAddress}/api/v1/users/0/orders/:orderID/
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名  | 必选 | 类型   | 限制条件 | 说明         |
| ------- | ---- | ------ | -------- | ------------ |
| orderID | 是   | string |          | 物品唯一标识 |

### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息   "data":     {        "buyerPhone":"159...",        "sellerPhone":"198..",        "orderStatus":0,        "orderPrice":10.0    }          // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](查看某个发布资源

# 确认（买家）/取消（买家）/审核（卖家）未成交的订单

### 1 接口名称

确认/取消/审核未成交的订单

### 2 接口描述

- 用户确认/取消/审核未成交订单
- 通过类型码来执行不同操作

### 3 请求地址

```
{apiAddress}/api/v1/users/0/orders/:orderID
```

### 4 请求方式

PATCH

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名  | 必选 | 类型   | 限制条件         | 说明             |
| ------- | ---- | ------ | ---------------- | ---------------- |
| type    | 是   | string | 确认\|取消\|审批 | 确认\|取消\|审批 |
| orderID | 是   | string |                  | 物品唯一标识     |
|         |      |        |                  |                  |



### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": null// 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](查看某个发布资源

# 查看资源主页

### 1 接口名称

查看所有资源

### 2 接口描述

- 用户可以通过此接口查看所有资源

### 3 请求地址

```
{apiAddress}/api/v1/items/
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名 | 必选 | 类型   | 限制条件           | 说明                                               |
| ------ | ---- | ------ | ------------------ | -------------------------------------------------- |
| type   | 是   | int    | 0\|1\|2\|3         | 查看所有\|查看公共资源\|查看个人资源\|查看个人能力 |
| key    | 否   | string | 0 < length < 30    | 查询关键字                                         |
| limit  | 是   | int    | default = 10000000 | 每页最多资源数                                     |
| page   | 是   | int    | default = 1        | 查看页数                                           |

### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": [    {        "itemsName":"dksfh",    "itemsPrice":10.0,    "itemsNumber":1,    "itemsMaxNum":10,    "remark":"hhh",    "itemsPublisher":"aaa"    } ,        {                    }        ...        ] // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](查看某个发布资源

# 搜索资源

### 1 接口名称

所有资源中搜索

### 2 接口描述

- 用户可以通过此接口搜索所有资源

### 3 请求地址

```
{apiAddress}/api/v1/items/search/
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 GET 参数

| 参数名 | 必选 | 类型   | 限制条件        | 说明       |
| ------ | ---- | ------ | --------------- | ---------- |
| key    | 否   | string | 0 < length < 30 | 查询关键字 |

### 6 返回示例

```json
{   "data": [    {        "itemsName":"dksfh",    "itemsPrice":10.0,    "itemsNumber":1,    "itemsMaxNum":10,    "remark":"hhh",    "itemsPublisher":"aaa"    } ,        {                    }        ...        ] // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](查看某个发布资源

# 查看某个资源

### 1 接口名称

查看某个已发布资源

### 2 接口描述

- 用户查看某个已发布资源

### 3 请求地址

```
{apiAddress}/api/v1/items/:itemsID
```

### 4 请求方式

GET

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 GET 参数

| 参数名  | 必选 | 类型   | 限制条件 | 说明         |
| ------- | ---- | ------ | -------- | ------------ |
| itemsID | 是   | string |          | 查看商品的ID |

### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": [    {    "itemsName":"dksfh",    "itemsPrice":10.0,    "itemsNumber":1,    "itemsMaxNum":10,    "itemsStartTime":"",    "itemsEndTime":"",    "remark":"hhh",    "itemsType": "私有",    "itemsPublisher":"aaa"    }         ] // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](查看某个发布资源

# 下订单

### 1 接口名称

下订单

### 2 接口描述

- 用户在指定资源页面进行下单

### 3 请求地址

```
{apiAddress}/api/v1/items/:itemsID
```

### 4 请求方式

POST

### 5 请求参数

#### 5.1 Header 参数

| 参数名       | 必选 | 类型/参数值      | 说明         |
| ------------ | ---- | ---------------- | ------------ |
| Content-Type | 是   | application/json | 请求参数类型 |

#### 5.2 Body 参数

| 参数名  | 必选 | 类型   | 限制条件 | 说明         |
| ------- | ---- | ------ | -------- | ------------ |
| itemsID | 是   | string | not null | 物品唯一标识 |
| buyNum  | 是   | int    | >0       | 购买数量     |

### 6 返回示例

```json
{    "code": 200,  // 状态码    "msg": "成功",  // 提示信息    "data": [    {        "ordersID":"11",        "sellerPhone":"198..",        "orderStatus":0,        "orderPrice":10.0    } ,        ] // 返回内容}
```



### 7 备注

更多返回错误码请查看首页返回状态码表

[接口返回状态码表](





# 实体类初定义

## users类

### 属性

| 名称        | 限制        | 类型   | 说明                   |
| :---------- | ----------- | ------ | ---------------------- |
| userPicture |             | string | 用户头像地址           |
| uuid        | primary key | string | 用户唯一编号           |
| username    | unique      | string | 用户电话号作为登陆账号 |
| password    | not null    | string | 登陆密码               |
| stuID       | unique      | string | 用户学号               |
| nickname    | not null    | string | 用户昵称               |
| userPro     | not null    | string | 密保问题               |
| userAns     | not null    | string | 密保答案               |
| userCredit  | not null    | int    | 信誉积分               |
| userMoney   | not null    | String | 虚拟货币/个            |

## items类

### 属性

| 名称           | 限制                         | 类型   | 说明                         |
| :------------- | ---------------------------- | ------ | ---------------------------- |
| itemsID        | primary key                  | int    | 物品唯一编号                 |
| itemsName      | not null                     | string | 物品名称                     |
| itemsNumber    | not null                     | int    | 物品剩余数量                 |
| itemsMaxNum    | not null                     | int    | 物品最大可并行数             |
| itemsPrice     | not null                     | double | 物品单价                     |
| itemsPublisher | foreign key ->users          | string | 物品发布者                   |
| itemsEndTime   | not null                     | date   | 租赁终止时间                 |
| itemsStartTime | not null                     | date   | 租赁起始时间                 |
| itemsType      | 公共资源\|个人资源\|个人能力 | string | 公共资源\|个人资源\|个人能力 |
| remark         |                              | string | 备注                         |
| itemsPicture   |                              | string | 物品实拍地址                 |



## orders类

### 属性

| 名称              | 限制                           | 类型   | 说明             |
| :---------------- | ------------------------------ | ------ | ---------------- |
| orderID           | primary key                    | string | 订单唯一编号     |
| buyer             | foreign key ->users            | string | 物品购买者       |
| seller            | foreign key ->users            | string | 物品提供者       |
| orderStatus       | 待付款\|审核中\|已完成\|已取消 | string | 订单状态         |
| orderPrice        | not null                       | double | 订单金额         |
| orderDate         | not null                       | date   | 订单生成时间     |
| orderitemsName    | not null                       | string | 物品名称         |
| orderItemsID      | not null                       | string | 物品的编号       |
| orderItemsNum     | not null                       | int    | 所购物品数量     |
| orderItemsPrice   | not null                       | double | 物品单价         |
| orderItemsPicture |                                | string | 订单显示图片地址 |



- 第1章 新版SSM-Springboot2.X-Spring5-Mybatis3专题课程介绍
- 1-1 新版Springboot2.X-Spring5-Mybatis3专题课程介绍
- 1-2 新版SSM+小滴课堂移动端系统实战+云服务器部署大纲速览
- 1-3 必备基础-15分钟认识Spring5框架和基本概念

- 第2章 微服务必备核心-快速入门SpringBoot2.X
- 2-1 你知道SpringBoot2.X和SpringCloud微服务的关系吗
- 2-2 新版SSM框架-SpringBoot2.X介绍和环境依赖
- 2-3 玩转SpringBoot2.X项目创建工具SpringInitializr
- 2-4 企业工作或者学习新技术，遇到问题推荐的解决方式
- 2-5 新版SpringBoot2.X开发第一个JSON接口
- 2-6 前后端分离测试必备工具PostMan接口工具使用
- 2-7 新版SpringBoot2.X的常用注解你知多少

- 第3章 互联网公司中新版SpringBoot2.X开发规范解读
- 3-1 新版SpringBoot2.x目录文件结构讲解和静态资源访问
- 3-2 新版SpringBoot2.x启动类位置常见形式
- 3-3 SpringBoot2.X启动方式讲解和部署
- 3-4 打包后的Jar里面的目录结构你知道多少

- 第4章 淘汰传统XX管理系统! 在线教育项目核心模块学SpringBoot2.X
- 4-1 小滴课堂在线教育项目原型需求分析
- 4-2 SpringBoot2.X在线教育项目架构搭建
- 4-3 SpringBoot2.X开发HTTP接口GET请求实战
- 4-4 SpringBoot2.X统一接口返回协议-掌握开发规范
- 4-5 SpringBoot2.X开发HTTP接口POST请求实战
- 4-6 实战RequestBody对象数组提交接口开发
- 4-7 SpringBoot2.X里面定制JSON字段

- 第5章 IDEA中SpringBoot2.X热部署Dev-tool和常见问题
- 5-1 什么是热部署，为啥要用这个呢
- 5-2 SpringBoot2.X结合dev-tool实现IDEA项目热部署

- 第6章 最佳实践之SpringBoot.2.X里面的配置文件
- 6-1 带你看SpringBoot2.x常见配置文件形式
- 6-2 新版SpringBoot注解配置文件映射属性和实体类实战

- 第7章 项目实战中的Springboot2.X单元测试应用
- 7-1 公司里面软件开发测试流程你知道多少
- 7-2 在线教育项目里面怎么用SpringBoot2.x的单元测试
- 7-3 案例实战单元测试调用Controller-Service层接口
- 7-4 MockMvc案例实战调用Controller层API接口

- 第8章 项目实战中的Springboot2.X全局异常处理
- 8-1 SpringBoot2.x在线教育项目中里面的全局异常处理
- 8-2 SpringBoot2.x自定义全局异常返回页面

- 第9章 新版Servlet3.0和SpringBoot2.X过滤器-拦截器实战
- 9-1 SpringBoot2.X过滤器讲解
- 9-2 抛弃传统方式，使用新版Servlet3.0的注解开发自定义Filter
- 9-3 前后端分离-自定义Filter未登录json错误码提示开发
- 9-4 回归本质Servlet3.0的注解原生Servlet实战
- 9-5 Servlet3.0+SpringBoot2.X注解Listener常用监听器
- 9-6 新版SpringBoot2.X拦截器配置实战

- 第10章 新版SpringBoot2.X 整合模板引擎thymeleaf和Fk
- 10-1 新版SpringBoot2.x的starter和常见模板引擎讲解
- 10-2 SpringBoot2.X整合模板引擎freemarker实战
- 10-3 SpringBoot2.X整合模板引擎thymeleaf实战

- 第11章 玩转新版SpringBoot2.X整合定时任务和异步任务
- 11-1 SpringBoot2.X定时任务schedule讲解
- 11-2 SpringBoot2.X多种定时任务配置实战
- 11-3 玩转SpringBoot2.x异步任务EnableAsync实战
- 11-4 玩转SpringBoot2.x异步任务Future实战

- 第12章 带你设计在线教育项目核心数据库表
- 12-1 在线教育项目核心数据库表设计-ER图
- 12-2 在线教育项目核心数据库表字段设计和测试数据准备

- 第13章 从Javaweb原生jdbc到快速掌握新版MyBatis3.X
- 13-1 回顾javaweb通过原生jdbc访问数据库
- 13-2 原生JDBC访问数据库的缺点和ORM框架介绍
- 13-3 新版ORM框架Mybatis3.X基础知识
- 13-4 新版Mybatis3.X快速入门实战《上》
- 13-5 新版Mybatis3.X快速入门实战《下》

- 第14章 案例实战MyBatis3.X玩转查询和新增操作
- 14-1 新版Mybatis开发必备调试之控制台打印Sql
- 14-2 Mybatis实战参数别名使用之查询视频列表
- 14-3 Mybatis配置驼峰字段映射java对象和数据库字段
- 14-4 Mybatis入参parameterType和取值类型讲解
- 14-5 Mybatis实战插入语法之视频新增和自增主键
- 14-6 Mybatis实战foreach批量插入语法之视频批量插入

- 第15章 案例实战MyBatis3.X玩转更新和删除
- 15-1 MyBatis3.X实战更新语法之视频更新操作
- 15-2 MyBatis3.X实战更新语法之选择性更新标签使用
- 15-3 MyBatis3.X实战之删除语法和转义字符使用

- 第16章 新版MyBatis3.X玩转常见配置
- 16-1 MyBatis3.X配置文件mybatis-config.xml常见属性
- 16-2 MyBatis3.X查询typeAlias别名的使用
- 16-3 高性能sql之MyBatis3.X的Sql片段使用

- 第17章 进阶MyBatis3.X复杂Sql查询
- 17-1 MyBatis3.X的resultMap你知道多少
- 17-2 ResultMap复杂对象一对一查询结果映射之association
- 17-3 ResultMap复杂对象一对多查询结果映射之collection
- 17-4 Mybatis3.XResultMap复杂对象查询总结

- 第18章 面试章-MyBatis3.X高手系列-玩转多级缓存和懒加载
- 18-1 Mybatis3.X一级缓存讲解和案例实战
- 18-2 你知道Mybatis3.X二级缓存怎么使用《上》
- 18-3 你知道Mybatis3.X二级缓存怎么使用《下》
- 18-4 你知道Mybatis3.X懒加载吗-看这个就懂了？

- 第19章 MyBatis3.X高手系列之整合Mysql数据库事务
- 19-1 新版MyBatis3.x的事务管理形式
- 19-2 面试题-Mysql的Innodb和MyISAM引擎的区别
- 19-3 新版MyBatis3.x的事务控制-走读源码和案例

- 第20章 新版SSM之Spring Framework5.X快速入门
- 20-1 快速认知-新版SpringFramework5.X
- 20-2 IDEA+Maven+Spring5.X项目创建
- 20-3 ApplicationContext.xml配置文件讲解和Helloworld例子解读
- 20-4 新版SpringFramework5.X核心之IOC容器讲解
- 20-5 新版SpringFramework5.X核心之DI依赖注入讲解

- 第21章 玩转Spring5.X bean 的作用域和注入
- 21-1 新版Spring5.x的bean的scope作用域讲解
- 21-2 实战Spring5.X常见的注入方式
- 21-3 实战Spring5.XList-Map类型的注入
- 21-4 玩转springioc容器Bean之间的依赖和继承

- 第22章 玩转Spring5.X bean 的生命周期和二次处理
- 22-1 玩转springioc容器Bean的生命周期的init和destroy方法
- 22-2 bean的二次加工-Spring5.x后置处理器BeanPostProcessor
- 22-3 Spring5.Xbean自动装配Autowire属性

- 第23章 高级知识点-玩转Spring5.X 面向切面编程 AOP
- 23-1 什么是AOP面向切面编程
- 23-2 掌握AOP面向切面编程核心概念
- 23-3 AOP里面的通知Advice类型讲解
- 23-4 举个通俗的栗子-AOP面向切面编程
- 23-5 Spring5.XAOP切入点表达式讲解

- 第24章 Spring AOP里面的代理知识你知道多少
- 24-1 你知道动态代理和静态代理吗
- 24-2 代理模式实战之静态代理讲解
- 24-3 AOP的实现策略之JDK动态代理
- 24-4 AOP的实现策略之CGLib动态代理
- 24-5 CGLib动态代理和JDK动态代理总结

- 第25章 面向切面编程 Spring AOP 实战 配置
- 25-1 基于Spring的AOP快速实现通用日志打印《上》
- 25-2 基于Spring的AOP快速实现通用日志打印《下》

- 第26章 玩转Spring5.X Xml配置转换到注解配置
- 26-1 论Spring使用方式之XML和注解的优缺点
- 26-2 实战Spring5.X的注解配置项目
- 26-3 实战Spring5.X的常用注解和xml对比《上》
- 26-4 实战Spring5.X的常用注解和xml对比《下》

- 第27章 玩转Spring5.X Xml配置转换到注解配置进阶
- 27-1 spring的@Configuration和@Bean注解定义第三方bean
- 27-2 Spring的自动映射配置文件PropertySource注解讲解

- 第28章 案例实战之基于Spring注解配置AOP面向切面编程
- 28-1 SpringAOP注解基础准备
- 28-2 开启SpringAOP注解配置和扫描
- 28-3 AOP案例实战之环绕通知统计接口耗时

- 第29章 新版SSM整合-打通Mysql数据库控制事务（包含面试题）
- 29-1 Spring常见的事务管理-面试常考点
- 29-2 Spring事务的传播属性和隔离级别
- 29-3 新版SpringBoot-Spring-Mybatsi事务控制讲解
- 29-4 新版SpringBoot-Spring-Mybatsi课程总结和后续内容预告

- 第30章 手机端-小滴课堂在线教育系统效果演示和技术准备
- 30-1 小滴课堂在线教育项目效果演示
- 30-2 轻量级-小滴课堂核心数据库表字段设计和测试数据准备
- 30-3 新版SSM-SpringBoot2.X后端项目框架搭建
- 30-4 小滴课堂综合实战-项目相关包和实体类创建

- 第31章 小滴课堂项目实战之打通Myabtis连接Mysql开发视频列表
- 31-1 小滴课堂实战之Mybaits打通Mysql数据库
- 31-2 小滴课堂实战之视频列表接口开发+API权限路径规划
- 31-3 小滴课堂实战之dev-tool实现IDEA项目热部署
- 31-4 小滴课堂实战之首页banner轮播图和视频详情接口开发
- 31-5 小滴课堂实战之视频详情接口开发-三表关联查询映射
- 31-6 小滴课堂实战之自定义异常开发和配置

- 第32章 小滴课堂项目实战之用户注册登录模块和JWT登录解决方案
- 32-1 小滴课堂实战之用户注册功能开发和MD5加密工具类封装
- 32-2 常见的互联网项目中单机和分布式应用的登录校验解决方案
- 32-3 分布式应用下登录检验解决方案JWT讲解
- 32-4 登录校验JsonWebToken实战之封装通用方法
- 32-5 小滴课堂实战之登录模块开发整合JsonWebToken

- 第33章 小滴课堂项目实战之登录拦截器开发和订单模块开发
- 33-1 小滴课堂实战之用户登录校验拦截器开发
- 33-2 小滴课堂实战之loginInterceptor注册和放行路径
- 33-3 小滴课堂实战之个人信息查询接口开发
- 33-4 小滴课堂实战之VideoOrder下单模块开发
- 33-5 小滴课堂实战之播放记录表设计和模块开发
- 33-6 小滴课堂实战之订单和播放记录事务控制
- 33-7 小滴课堂实战之订单列表接口开发

- 第34章 性能优化实战之协议优化和引入Guava缓存
- 34-1 小滴课堂实战之接口协议调整和日期格式
- 34-2 高并发项目必备利器之分布式缓存和本地缓存
- 34-3 谷歌开源缓存框架GuavaCache讲解和封装缓存组件
- 34-4 小滴课堂实战之轮播图接口引入本地缓存
- 34-5 小滴课堂实战之视频列表引入本地缓存
- 34-6 小滴课堂实战之视频详情引入本地缓存

- 第35章 压力测试工具Jmeter5.X快速入门实战
- 35-1 接口压测和常用压力测试工具对比
- 35-2 压测工具本地快速安装Jmeter5.x
- 35-3 Jmeter5.x目录文件讲解和汉化操作
- 35-4 Jmeter5.X基础功能组件介绍线程组和Sampler
- 35-5 Jmeter5.x实战之压测结果聚合报告分析

- 第36章 实战接口压力测试,明白优化前后的QPS并发差距和跨域配置
- 36-1 开启Guava缓存压测热点数据接口
- 36-2 取消Guava缓存压测热点数据接口和前后对比
- 36-3 SpringBoot2.X开启跨域配置

- 第37章 全栈工程师必备VSCODE编辑器和开发环境搭建
- 37-1 前端主流编辑器VSCode安装
- 37-2 互联网公司前端开发环境搭建之Node和Npm介绍
- 37-3 切换npm镜像源为淘宝npm镜像
- 37-4 新版Vue+脚手架Vue-Cli4.3安装
- 37-5 VsCode导入小滴课堂前端项目

- 第38章 全栈工程师必备之新版Vue2.6 急速掌握核心内容
- 38-1 新版VueCli4.3创建vue项目
- 38-2 新版Vue快速入门之Vue指令和参数
- 38-3 新版Vue快速入门之v-if和v-else条件指令
- 38-4 新版Vue快速入门之v-for循环指令
- 38-5 新版Vue快速入门之v-model
- 38-6 新版Vue快速入门之v-on监听事件
- 38-7 Vue常见缩写v-bind和v-on讲解
- 38-8 新版Vue快速入门之component组件
- 38-9 新版Vue快速入门之prop向子组件传值

- 第39章 小滴课堂前端项目技术栈介绍和ES6知识点补充
- 39-1 小滴课堂前端项目技术组件概述
- 39-2 ECMAScript6常见语法快速入门《上》
- 39-3 ECMAScript6常见语法快速入门《下》

- 第40章 全栈工程师之小滴课堂前端项目架构搭建
- 40-1 VueCli4.3搭建小滴课堂前端项目架构
- 40-2 小滴课堂前端项目目录结构创建和讲解
- 40-3 基于浏览器和node.js的http客户端Axios讲解
- 40-4 Axios封装通用后端请求API模块
- 40-5 Vue-Router开发小滴课堂前端项目路由

- 第41章 小滴课堂前端项目实战之通用底部选项卡CommonsFooter开发
- 41-1 怎样通过看Cube-UI官方文档和底部选项卡组件讲解
- 41-2 小滴课堂移动端cube-tab-bar选项卡开发《上》
- 41-3 小滴课堂移动端cube-tab-bar选项卡开发《下》

- 第42章 小滴课堂前端项目实战之首页轮播图和视频列表开发
- 42-1 小滴课堂首页Home模块开发
- 42-2 小滴课堂首页轮播图banner模块开发
- 42-3 小滴课堂首页视频列表模块开发

- 第43章 小滴课堂前端项目实战之视频详情页模块开发
- 43-1 小滴课堂视频详情页基础模块开发
- 43-2 小滴课堂视频详情页Header子组件开发
- 43-3 小滴课堂视频详情页Course子组件开发
- 43-4 小滴课堂视频详情页tab子组件开发和vue动态组件讲解
- 43-5 小滴课堂视频详情页summary子组件开发
- 43-6 小滴课堂视频详情页Catalog目录子组件开发
- 43-7 小滴课堂视频详情页footer立刻购买按钮开发

- 第44章 小滴课堂前端项目实战之注册-登录-个人中心模块开发
- 44-1 小滴课堂前端用户模块之注册功能开发
- 44-2 小滴课堂前端用户模块之登录功能开发
- 44-3 你知道vue里面的状态管理vuex吗
- 44-4 小滴课堂前端用户模块之个人中心开发

- 第45章 小滴课堂前端项目实战之路由拦截和订单模块开发
- 45-1 小滴课堂前端项目实战之路由拦截功能开发
- 45-2 小滴课堂前端下单页面组件开发
- 45-3 小滴课堂前端订单列表模块开发
- 45-4 小滴课堂前端项目开发总结回顾

- 第46章 互联网公司 前端-后端项目云服务器生产环境部署核心知识
- 46-1 小滴课堂互联网架构之应用部署上线核心知识
- 46-2 云服务器介绍和阿里云服务器ECS服务器选购
- 46-3 阿里云服务器远程登录和常用工具

- 第47章 生产环境Linux CentOS云服务器常见相关软件安装
- 47-1 阿里云Linux服务器Centos7安装JDK8环境
- 47-2 生产环境Linux服务器上Nginx介绍和安装
- 47-3 生产环境Linux服务器上Mysql安装和导入数据

- 第48章 小滴课堂综合项目实战-前端-后端集群部署线上Linux云服务器
- 48-1 前后端项目总体部署架构和阿里云域名解析A记录配置
- 48-2 后端JavaAPI项目阿里云服务器部署安装
- 48-3 前后端分离-前端项目打包上传阿里云服务器
- 48-4 线上部署之前端项目接入Nginx代理服务器
- 48-5 高可用处理之后端API多节点集群部署
- 48-6 小滴课堂前端-后端项目阿里云服务器部署总结

- 第49章 新版SSM课程零基础到项目实战总结和学习路线规划
- 49-1 新版SSM零基础到项目实战课程总结和学习路线推荐
- 49-2 高级工程师到架构师-解决问题思路+学习方法
